FUNCTION_BLOCK functionBlock0
  VAR_INPUT
    b0 : BOOL;
    b1 : BOOL;
  END_VAR
  VAR_OUTPUT
    l0 : BOOL := FALSE;
    l1 : BOOL := FALSE;
    l2 : BOOL := FALSE;
  END_VAR
  VAR_INPUT
    compteurIn : INT;
  END_VAR
  VAR_OUTPUT
    compteurOut : INT;
  END_VAR
  VAR
    compteurLocal : INT;
  END_VAR

  IF b0 = TRUE THEN
    compteurOut := compteurIn + 1;
  END_IF;
  IF b1 = TRUE THEN
    compteurOut := compteurIn - 1;
  END_IF;
  compteurLocal := compteurOut;
  IF compteurLocal / 4 > 1 THEN
    l0 := TRUE;
    compteurLocal := compteurLocal - 4;
  END_IF;
  IF compteurLocal / 2 > 1 THEN
    l1 := TRUE;
    compteurLocal := compteurLocal - 2;
  END_IF;
  IF compteurLocal > 1 THEN
    l2 := TRUE;
    compteurLocal := compteurLocal - 1;
  END_IF;
END_FUNCTION_BLOCK

PROGRAM My_Program
  VAR
    buttonPlus AT %QX0.2 : BOOL;
    buttonMinus AT %QX0.3 : BOOL;
    lamp0 AT %IX0.3 : BOOL;
    lamp1 AT %IX0.4 : BOOL;
    lamp2 AT %IX0.5 : BOOL;
  END_VAR
  VAR
    Compteur : INT;
    functionBlock00 : functionBlock0;
    TOF0 : TOF;
    TOF1 : TOF;
    TOF2 : TOF;
  END_VAR

  functionBlock00(b0 := buttonPlus, b1 := buttonMinus, compteurIn := Compteur);
  TOF0(IN := functionBlock00.l0, PT := T#1000ms);
  lamp0 := TOF0.Q;
  TOF1(IN := functionBlock00.l1, PT := T#1000ms);
  lamp1 := TOF1.Q;
  TOF2(IN := functionBlock00.l2, PT := T#1000ms);
  lamp2 := TOF2.Q;
  Compteur := functionBlock00.compteurOut;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK TaskMain(INTERVAL := T#50ms,PRIORITY := 0);
    PROGRAM Inst0 WITH TaskMain : My_Program;
  END_RESOURCE
END_CONFIGURATION
