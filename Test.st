PROGRAM My_Program
  VAR
    button0 AT %IX1.1 : BOOL;
    button1 AT %IX1.2 : BOOL;
    xorlamp AT %IX0.3 : BOOL;
    orlamp AT %IX0.4 : BOOL;
    andlamp AT %IX0.5 : BOOL;
  END_VAR
  VAR
    AND8_OUT : BOOL;
    OR9_OUT : BOOL;
    XOR10_OUT : BOOL;
  END_VAR

  AND8_OUT := AND(button0, button1);
  andlamp := AND8_OUT;
  OR9_OUT := OR(button0, button1);
  orlamp := OR9_OUT;
  XOR10_OUT := XOR(button0, button1);
  xorlamp := XOR10_OUT;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK TaskMain(INTERVAL := T#50ms,PRIORITY := 0);
    PROGRAM Inst0 WITH TaskMain : My_Program;
  END_RESOURCE
END_CONFIGURATION
